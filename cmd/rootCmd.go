package cmd

import (
	"github.com/spf13/cobra"
)

var (
	version string = "unset"

	rootCmd = &cobra.Command{
		Use:     "tbcctl",
		Short:   "A collection of tools to play with to be continuous.",
		Version: version,
	}
)

// Execute executes the root command.
func Execute() error {
	return rootCmd.Execute()
}
