all: linux windows

export GO_BUILD_LINKER_FLAGS :=  -ldflags "-X to-be-continuous/tbcctl/cmd.version=dev "

windows:
	mkdir -p bin/windows/amd64
	GOOS=windows GOARCH=amd64 go build $(TAGS) $(GO_BUILD_LINKER_FLAGS) -o bin/windows/amd64 ./...

linux:
	mkdir -p bin/linux/amd64
	go build $(TAGS) $(GO_BUILD_LINKER_FLAGS) -o bin/linux/amd64 ./...

test:
	go test -v ./...

clean:
	go clean

version:
	go version
