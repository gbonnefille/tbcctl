package main

import (
	"fmt"
	"os"
	"to-be-continuous/tbcctl/cmd"
)

func main() {
	if err := cmd.Execute(); err != nil {
		fmt.Fprintf(os.Stderr, "%s", err)
		os.Exit(1)
	}
}
